/**
 * Copyright 2013 William Meng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/**
 * A class to easily compare objects by priority, for use with PriorityQueue.
 * <p>
 * Essentially, a datum of parameterized-type E is mapped to a priority of integral value,
 * which is suitable for organization by a heap.
 * <p>
 * FUN FACT: The word "Queueable" contains 5 consecutive vowels.
 *
 * @author William Meng
 */
public abstract class Queueable<E> implements Comparable<Queueable<E>> {
	private static final int DEFAULT_PRIORITY = -1;
	private int priority;
	private E datum;

	Queueable(E datum) {
		this(datum, DEFAULT_PRIORITY);
	}
	Queueable(E datum, int priority) {
		this.datum = datum;
		setPriority(priority);
	}

	// Accessor methods
	public final int getPriority() { return priority; }
	public final void setPriority(int priority) { this.priority = priority; }

	public final E getDatum() { return datum; }
	public final void setDatum(E datum) { this.datum = datum; }

	/**
	 * Compares the priorities of the Queueable objects
	 */
	@Override
	public final int compareTo(Queueable<E> o1) {
		Integer i = priority; // the joy of Java
		return i.compareTo(o1.getPriority());
	}

	@Override
	public final String toString() {
		return "[" + priority + ", " + datum.toString() + "]";
	}
}
