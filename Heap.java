/**
 * Copyright 2013 William Meng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A binary max-heap, backed by an ArrayList.
 *
 * @author William Meng
 */
public class Heap<E extends Comparable<E>> {
	private ArrayList<E> arr;

	/**
	 * Constructs an empty heap
	 */
	public Heap() {
		arr = new ArrayList<E>();
	}
	/**
	 * Constructs a heap by inserting all the elements from a given array
	 */
	public Heap(E[] arr) {
		this.arr = new ArrayList<E>(arr.length);
		for (E e : arr)
			insert(e);
	}
	/**
	 * Ensure a minimum capacity when initializing the ArrayList
	 * @param	size	minimum capacity
	 */
	public Heap(int size) {
		arr = new ArrayList<E>(size);
	}


	// BEGIN utility methods
	@Override
	public String toString() {
		return arr.toString();
	}
	/**
	 * Prints the heap
	 */
	public void print() {
		System.out.println(toString());
	}
	/**
	 * Return number of elements in the heap
	 */
	public int size() {
		return arr.size();
	}
	public Iterator<E> iterator() {
		return arr.iterator();
	}
	// END utility methods

	/**
	 * Insert an element to the heap
	 * If the element already exists in the heap, return false
	 */
	public boolean insert(E datum) {
		if (arr.contains(datum))
			return false;
		arr.add(datum);
		siftUp(arr.size()-1);
		return true;
	}

	// BEGIN helper methods for insert()
	/**
	 * Maintain heap property when a node has been inserted
	 * @param 	i 	node index to swap
	 */
	private void siftUp(int i) {
		int p; // parent index
		if (i != 0) {
			p = parentIndex(i);
			if (arr.get(p).compareTo(arr.get(i)) < 0) {
				swap(p, i);
				siftUp(p);
			}
		}
	}

	/**
	 * Swaps two elements at the given index in the ArrayList
	 * @param 	a 	first index
	 * @param 	b 	second index
	 */
	private void swap(int a, int b) {
		E e1 = arr.get(a);
		E e2 = arr.get(b);
		arr.set(a, e2);
		arr.set(b, e1);
	}

	/**
	 * Returns the index of the element's parent
	 * @param 	i 	child index
	 * @param 	e 	child "node"
	 */
	private int parentIndex(int i) {
		return (i-1)/2;
	}
	private int parentIndex(E e) {
		int i = arr.indexOf(e);
		return (i-1)/2;
	}
	// END helper methods for insert()

	/** Remove and return a node at a given index */
	public E remove(int index) {
		if (arr.size() == 1)
			return arr.remove(index);
		if (!arr.isEmpty()) {
			E ret = arr.get(index); // keep a copy of root to return
			E last = arr.remove(arr.size()-1);
			arr.set(index, last); // move last node to beginning
			if (!arr.isEmpty())
				siftDown(index);
			return ret;
		}
		return null;
	}
	/** Remove and return a given node, if it exists */
	public E remove(E datum) {
		if (arr.contains(datum))
			return remove(arr.indexOf(datum));
		return null;
	}
	/** Remove and return the root node */
	public E remove() {
		return remove(0);
	}

	// BEGIN helper methods for delete()
	private void siftDown(int i) {
		int max; // index of element to swap with
		// child indices
		int c1 = leftChildIndex(i);
		int c2 = rightChildIndex(i);

		if (c2 >= arr.size()) {
			if (c1  >= arr.size())
				return; // leaf node
			max = c1;
		}
		else // set max to the greater of the children
			max = (arr.get(c1).compareTo(arr.get(c2)) > 0)? c1 : c2;
		if (arr.get(i).compareTo(arr.get(max)) < 0) {
			swap(max, i);
			siftDown(max);
		}
	}

	private int leftChildIndex(int i) {
		return 2*i + 1;
	}
	private int rightChildIndex(int i) {
		return 2*i + 2;
	}
	// END helper methods for delete()

	/**
	 * Retrieve a node without deleting it
	 */
	public E peek() {
		return peek(0);
	}
	public E peek(int index) {
		if (!arr.isEmpty())
			return arr.get(index);
		return null;
	}
	public E peek(E datum) {
		return peek(arr.indexOf(datum));
	}

	/**
	 * Reset to an empty Heap
	 */
	public void clear() {
		arr.clear();
	}

	public static void main(String[] args) {
		Heap<Integer> h = new Heap<>();
		int[] arr = {5, 2, 4, 13, 1, 24, 12, 54, 14, 18, 3};
		for (int i : arr) {
			h.insert(i);
			h.print();
		}

		h.clear();
		arr = new int[] {6, 3, 5, 2, 4};
		for (int i : arr) {
			h.insert(i);
			h.print();
		}
	}
	/**
	 * Given an array, returns a heap
	 */
	public static <E extends Comparable<E>> Heap<E>
		heapify(E[] arr) {
			return new Heap<E>(arr);
		}
}

/*
 * Possible efficiently mergeable implementations:
 * Binomial
 * Binary (slow)
 * Pairing (good practical amortized, unknown strict asymptotic)
 * Fibonacci
 * Brodal (complicated in real world)
 * Strict Fibonacci
 * Rank Pairing
 * Skew (faster than binary)
 */
