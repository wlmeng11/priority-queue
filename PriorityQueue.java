/**
 * Copyright 2013 William Meng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

import java.util.AbstractQueue;
import java.util.Iterator;

/**
 * A priority queue class, backed by a binary max-heap.
 * <p>
 * Elements are ordered by natural ordering from greatest to least,
 * using the <code>compareTo()</code> method.
 *
 * To associate unordered data with a priority, use the Queueable class.
 *
 * @author William Meng
 */
public class PriorityQueue<E extends Comparable<E>>
	extends AbstractQueue<E> {

	private static final int DEFAULT_CAPACITY = 12;
	private Heap<E> heap;

	/**
	 * Constructs a heap with the default capacity
	 */
	public PriorityQueue() {
		heap = new Heap<E>(DEFAULT_CAPACITY);
	}
	/**
	 * Constructs a heap containing all the elements in the given array
	 */
	public PriorityQueue(E[] arr) {
		heap = new Heap<E>(arr);
	}
	/**
	 * Ensure a minimum capacity when initializing the heap
	 * @param	size	minimum capacity
	 */
	public PriorityQueue(int size) {
		heap = new Heap<E>(size);
	}

	// BEGIN utility methods
	@Override
	public String toString() {
		return heap.toString();
	}

	/**
	 * Prints the queue
	 */
	public void print() {
		System.out.println(toString());
	}

	@Override
	public int size() {
		return heap.size();
	}
	@Override
	public Iterator<E> iterator() {
		return heap.iterator();
	}
	// END utility methods

	/**
	 * Because the backing heap is based on an ArrayList, this method is identitical to add()
	 */
	@Override
	@Deprecated
	public boolean offer(E e) {
		return add(e);
	}

	/**
	 * Return the head of the queue without removing it.
	 */
	@Override
	public E peek() {
		return heap.peek();
	}

	/**
	 * Remove and return the head of the queue.
	 */
	@Override
	public E poll() {
		return heap.remove();
	}

	/**
	 * Insert element to appropriate position in the queue.
	 */
	@Override
	public boolean add(E e) {
		return heap.insert(e);
	}

	/**
	 * Code to test functionality
	 */
	public static void main(String[] args) {
		System.out.println("An example with Integers. The priority is the value of the Integer");
		PriorityQueue<Integer> q = new PriorityQueue<>();
		int[] arr = {5, 2, 4, 13, 1, 24, 12, 54, 14, 18, 3}; // populate in arbitrary order
		for (int i : arr)
			q.add(i);
		System.out.println("\"pop\" each element from the queue");
		while (q.size() > 0)
			System.out.print(q.poll() + " "); // printed in natural order, from greatest to least
		System.out.println();
		System.out.println("alternatively, just print the entire data structure");
		System.out.println(q);
		System.out.println("just kidding, we already de-queued everything, so the heap is empty");
		System.out.println();

		System.out.println("An example with Strings.\nFirst, the priority is printed, then the String.");
		PriorityQueue<Queueable<String>> pq = new PriorityQueue<>();
		Foo a = new Foo("IT'S OVER 9000! this is message a", 9001);
		Foo b = new Foo("this is message b", 100);
		Foo c = new Foo("this is message c"); // with default priority for a Queueable object
		pq.add(b); // add b first
		pq.add(c);
		pq.add(a);
		while (pq.size() > 0) {
			System.out.println(pq.poll()); // should print a first
		}
	}

	/**
	 * A Queueable String class for testing
	 * No added functionality to Queueable, but reduces some excessive typing in main
	 */
	private static class Foo extends Queueable<String> {
		Foo(String s) {
			super(s);
		}
		Foo(String s, int priority) {
			super(s, priority);
		}
	}
}
